const express = require('express');
const path = require('path');
const fs = require('fs')

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Serve static files from the assets directory
app.use(express.static(path.join(__dirname, 'assets')));

app.get('/', (req, res) => {
    res.render('pages/login')
})

// Function to create routes based on files in views/pages
const createRoutes = () => {
    const pagesDir = path.join(__dirname, 'views/pages');

    fs.readdir(pagesDir, (err, files) => {
        if (err) throw err;

        files.forEach(file => {
            const route = file.replace('.ejs', '');
            const routePath = `/${route}`;

            app.get(routePath, (req, res) => {
                res.render(`pages/${route}`);
            })
        })
    })
}

createRoutes();

// Start the server
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});