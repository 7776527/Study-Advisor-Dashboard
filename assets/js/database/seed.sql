-- Insert sample students
INSERT INTO students (name, student_number) VALUES
('Anna Nas', '123456'),
('Obiwan Kenoobi', '654321'),
('Alice Johnson', '789012'),
('Bob Brown', '345678');

-- Insert sample courses
INSERT INTO courses (name) VALUES
('Math'),
('Science'),
('History'),
('Literature');

-- Insert sample student_courses
INSERT INTO student_courses (student_id, course_id) VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(3, 3),
(3, 4),
(4, 1),
(4, 4);

-- Insert sample advisors
INSERT INTO advisors (name, email, password) VALUES
('Patrick Star', 'p.star@uu.nl', 'password123'),
('Ion Know', 'i.know@uu.nl', 'password123'),
('Blue Dabee', 'b.dabee@uu.nl', 'password123');

-- Insert sample todos
INSERT INTO todos (advisor_id, task, completed) VALUES
(1, 'Review Anna Nas\'s progress', false),
(1, 'Prepare materials for Science course', true),
(2, 'Schedule meeting with Obiwan Kenoobi', false),
(3, 'Evaluate assignments for Literature', false);

-- Insert sample meetings
INSERT INTO meetings (advisor_id, student_id, meeting_date, location) VALUES
(1, 1, '2024-06-24 10:00:00', 'BBG 101'),
(1, 2, '2024-06-25 11:00:00', 'BBG 101'),
(2, 3, '2024-06-25 14:00:00', 'KBG 102'),
(3, 4, '2024-06-27 09:00:00', 'KBG 103');

-- Insert sample notes
INSERT INTO notes (advisor_id, student_id, content) VALUES
(1, 1, 'Anna Nas is making good progress in Math and Science.'),
(1, 2, 'Obiwan Kenoobi needs to improve attendance.'),
(2, 3, 'Alice Johnson showed great interest in History.'),
(3, 4, 'Bob Brown is excelling in Literature.');
